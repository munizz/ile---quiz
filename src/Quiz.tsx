import React from "react";
import { Card, Row, Button, Col, Input } from "antd";
import { quiz, QuizProps } from "./mocks/quiz";

const QuizView = ({ data }: { data: QuizProps }) => {
  return (
    <div className="App">
      <Card title={data.category || "NONE"}>
        <Row>
          <h1>{data ? data.question : ""}</h1>
        </Row>
        <br />
        <Row>
          <Col span={12}>
            <p>{data ? data.options[0] : ""}</p>
          </Col>
          <Col span={12}>
            <p>{data ? data.options[1] : ""}</p>
          </Col>
        </Row>
        <Row>
          <Col span={12}>
            <p>{data ? data.options[0] : ""}</p>
          </Col>
          <Col span={12}>
            <p>{data ? data.options[1] : ""}</p>
          </Col>
        </Row>
        <Row style={{ marginTop: "125px", textAlign: "center" }}>
          <Col span={6}>
            <Input name="answer" />
          </Col>
          <Col span={6}>
            <Button type="primary">Answer</Button>
          </Col>
        </Row>
      </Card>
    </div>
  );
};

export default QuizView;
