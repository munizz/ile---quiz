import React, { useState, useEffect } from "react";
import "./App.css";

import {
  Layout,
  Menu,
  Breadcrumb,
  Icon,
  Button,
  Card,
  Row,
  Col,
  Input
} from "antd";
import { quiz, QuizProps } from "./mocks/quiz";
import QuizView from "./Quiz";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const App = () => {
  const [category, setCategory] = useState<string>("");
  const [level, setLevel] = useState<string>("");
  const [question, setQuestion] = useState<QuizProps>();
  const [usedData, setUsedData] = useState<number[]>([]);

  useEffect(() => {
    setQuestion(quiz.find(f => f.category === category && f.level === level));
  }, [category, level]);

  const handleSubmit = (submittedData: QuizProps) => {
    setUsedData([...usedData, submittedData.id]);
  };

  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Sider>
        <div className="logo" />

        <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
          {/* Xulambs Title */}
          <Menu.Item key="1">
            <Icon type="pie-chart" />
            <span>Xulambs Quiz</span>
          </Menu.Item>

          {/* Easy Mode */}
          <SubMenu
            key="easyMode"
            onTitleClick={() => setLevel("easy")}
            title={
              <span>
                <Icon type="team" />
                <span>Easy Mode</span>
              </span>
            }
          >
            <Menu.Item
              key="2"
              title="Tip - 10 notes"
              onClick={() => setCategory("listening")}
            >
              Music - What's the Song?
            </Menu.Item>
            <Menu.Item key="3" onClick={() => setCategory("quiz")}>
              Quiz
            </Menu.Item>
          </SubMenu>

          {/* Normal Mode */}
          <SubMenu
            key="normalMode"
            onTitleClick={() => setLevel("normal")}
            title={
              <span>
                <Icon type="team" />
                <span>Normal Mode</span>
              </span>
            }
          >
            <Menu.Item
              key="4"
              title="Tip - 7 notes"
              onClick={() => setCategory("listening")}
            >
              Music - What's the Song?
            </Menu.Item>
            <Menu.Item key="5" onClick={() => setCategory("quiz")}>
              Quiz
            </Menu.Item>
          </SubMenu>

          {/* Hard Mode */}
          <SubMenu
            key="hardMode"
            onTitleClick={() => setLevel("hard")}
            title={
              <span>
                <Icon type="team" />
                <span>Hard Mode</span>
              </span>
            }
          >
            <Menu.Item
              key="6"
              title="Tip - 10 notes"
              onClick={() => setCategory("listening")}
            >
              Music - What's the Song?
            </Menu.Item>
            <Menu.Item key="7" onClick={() => setCategory("singing")}>
              Music - Let's sing a little bit?
            </Menu.Item>
            <Menu.Item key="8" onClick={() => setCategory("quiz")}>
              Quiz
            </Menu.Item>
          </SubMenu>
        </Menu>
      </Sider>

      <Layout>
        <Header style={{ background: "#fff", padding: 0 }} />
        <Content style={{ margin: "0 16px" }}>
          <Breadcrumb style={{ margin: "16px 0" }}>
            <Breadcrumb.Item>{category}</Breadcrumb.Item>
            <Breadcrumb.Item>{level}</Breadcrumb.Item>
          </Breadcrumb>
          {question && <QuizView data={question} />}
        </Content>
        <Footer style={{ textAlign: "center" }}>Namekusei 2019</Footer>
      </Layout>
    </Layout>
  );
};

export default App;
