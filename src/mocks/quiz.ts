export interface QuizProps {
  id: number;
  category: string;
  level: string;
  question: string;
  answer: string;
  options: string[];
  reward: string;
}

export const quiz: QuizProps[] = [
  {
    id: 0,
    category: "listening",
    level: "easy",
    question: "Easy Listening",
    options: ["Xuxa", "Pele (Do you understand)"],
    answer: "Galvao Bueno",
    reward: "chocolate (1x)"
  },
  {
    id: 1,
    category: "listening",
    level: "normal",
    question: "Normal Listening",
    options: ["Xuxa", "Pele (Do you understand)"],
    answer: "Galvao Bueno",
    reward: "chocolate (1x)"
  },
  {
    id: 2,
    category: "singing",
    level: "easy",
    question: "Easy Singing",
    options: ["Xuxa", "Pele (Do you understand)"],
    answer: "Galvao Bueno",
    reward: "chocolate (1x)"
  },
  {
    id: 3,
    category: "singing",
    level: "easy",
    question: "Hard Singing",
    options: ["Xuxa", "Pele (Do you understand)"],
    answer: "Galvao Bueno",
    reward: "chocolate (1x)"
  },
  {
    id: 4,
    category: "quiz",
    level: "easy",
    question: "dsapkfughsd",
    options: ["Xuxa", "Pele (Do you understand)"],
    answer: "Galvao Bueno",
    reward: "chocolate (1x)"
  }
];
