import React from "react";
import logo from "./logo.svg";
import { Layout } from "antd";

const { Header, Footer, Sider, Content } = Layout;

const Home = () => {
	return (
		<div className="App">
			<Layout>
				<Header>Header</Header>
				<Content>Content</Content>
				<Footer>Footer</Footer>
			</Layout>
		</div>
	);
};

export default Home;
