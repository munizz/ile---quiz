import React from "react";
import { Layout, Card, Row, Button, Col, Input } from "antd";
import { QuizProps } from "./mocks/quiz";
import { quiz } from "./mocks/quiz";

const { Header, Footer, Sider, Content } = Layout;

const ListenerView = () => {
	const data = quiz.find(d => d.id === 0);

	return (
		<div className="App">
			<Card title="It's time to listen a beeeautiful song =)">
				<Row>
					<h1>{data ? data.question : ""}</h1>
				</Row>
				<br />
				<Row>
					<Col span={12}>
						<p>{data ? data.options[0] : ""}</p>
					</Col>
					<Col span={12}>
						<p>{data ? data.options[1] : ""}</p>
					</Col>
				</Row>
				<Row>
					<Col span={12}>
						<p>{data ? data.options[0] : ""}</p>
					</Col>
					<Col span={12}>
						<p>{data ? data.options[1] : ""}</p>
					</Col>
				</Row>
				<Row style={{ marginTop: "125px", textAlign: "center" }}>
					<Col span={6}>
						<Input name="answer" />
					</Col>
					<Col span={6}>
						<Button type="primary">Answer</Button>
					</Col>
				</Row>
			</Card>
		</div>
	);
};

export default ListenerView;
